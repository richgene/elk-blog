create database `elk-blog` default character set utf8mb4 collate utf8mb4_general_ci if not exists `juzizhou`;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '123456' WITH GRANT OPTION;
flush privileges;
use juzizhou;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tk_about
-- ----------------------------
DROP TABLE IF EXISTS `tk_about`;
CREATE TABLE `tk_about`  (
                             `id` int(10) NOT NULL,
                             `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
                             `status` tinyint(1) NULL DEFAULT 1,
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `update_time` datetime(0) NULL DEFAULT NULL,
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tk_article
-- ----------------------------
DROP TABLE IF EXISTS `tk_article`;
CREATE TABLE `tk_article`  (
                               `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
                               `catid` smallint(5) UNSIGNED NULL DEFAULT NULL,
                               `title` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
                               `md` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
                               `keywords` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `smallimg` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `ctime` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `username` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `author` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `listorder` smallint(5) UNSIGNED NULL DEFAULT 0,
                               `posid` tinyint(4) NOT NULL DEFAULT 0,
                               `ord` tinyint(3) NULL DEFAULT NULL,
                               `views` int(10) NULL DEFAULT NULL COMMENT '浏览量',
                               `likes` int(10) NULL DEFAULT NULL COMMENT '点赞',
                               `source_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `avatar` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `status` tinyint(4) NULL DEFAULT 0,
                               `create_time` datetime(0) NULL DEFAULT NULL,
                               `update_time` datetime(0) NULL DEFAULT NULL,
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 103 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tk_banner
-- ----------------------------
DROP TABLE IF EXISTS `tk_banner`;
CREATE TABLE `tk_banner`  (
                              `id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
                              `modelId` int(10) NOT NULL,
                              `smallimg` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                              `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                              `link` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                              `ord` int(8) NULL DEFAULT NULL,
                              `status` tinyint(1) NULL DEFAULT 1,
                              `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                              `update_time` datetime(0) NULL DEFAULT NULL,
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tk_category
-- ----------------------------
DROP TABLE IF EXISTS `tk_category`;
CREATE TABLE `tk_category`  (
                                `colId` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
                                `colPid` smallint(5) NOT NULL,
                                `typeid` smallint(3) NOT NULL DEFAULT 0,
                                `modelid` smallint(5) NOT NULL,
                                `model` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                `picId` smallint(5) NULL DEFAULT NULL,
                                `colPath` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                `colTitle` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                `thumb` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                                `asmenu` tinyint(1) NULL DEFAULT NULL,
                                `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                `status` tinyint(2) NULL DEFAULT NULL,
                                `ord` smallint(3) NULL DEFAULT NULL,
                                `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                `update_time` datetime(0) NULL DEFAULT NULL,
                                PRIMARY KEY (`colId`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 204 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tk_category
-- ----------------------------
INSERT INTO `tk_category` VALUES (1, 0, 0, 1, 'Design', 0, '0', 'Design', '', 1, '设计1', 1, 0, NULL, '2023-01-15 14:18:37');
INSERT INTO `tk_category` VALUES (2, 0, 0, 2, 'About', 0, '0', 'About', '', 1, '关于我们', 1, 0, NULL, '2023-01-15 14:18:37');
INSERT INTO `tk_category` VALUES (3, 0, 0, 3, 'Contact', 0, '0', 'Contact', '', 1, '联系我', 1, 0, NULL, '2023-01-15 14:18:37');
INSERT INTO `tk_category` VALUES (4, 0, 0, 4, 'Blog', 0, '0', 'Blog', '', 1, '博客', 1, 0, NULL, '2023-01-15 14:18:37');
INSERT INTO `tk_category` VALUES (5, 1, 0, 1, 'Design', 0, '0-1', 'GUI444', '', 0, '<p>ios android</p>', 1, 1, NULL, '2023-01-15 14:18:37');
INSERT INTO `tk_category` VALUES (6, 1, 0, 1, 'Design', 0, '0-1', 'WEB', '', 0, 'WEB', 1, 4, NULL, '2023-01-15 14:18:37');
INSERT INTO `tk_category` VALUES (7, 1, 0, 1, 'Design', 0, '0-1', 'PAINT', '', 0, 'PAINT', 1, 3, NULL, '2023-01-15 14:18:37');
INSERT INTO `tk_category` VALUES (17, 4, 0, 4, 'Blog', 0, '0-4', '轻慢摄影', '', 0, '轻慢摄影', 0, 0, NULL, '2023-01-15 14:18:37');
INSERT INTO `tk_category` VALUES (14, 4, 0, 4, 'Blog', 0, '0-4', '快乐绘画', '', 0, '快乐绘画', 0, 0, NULL, '2023-01-15 14:18:37');
INSERT INTO `tk_category` VALUES (15, 4, 0, 4, 'Blog', 0, '0-4', '收藏点滴', '', 0, '收藏点滴', 0, 0, NULL, '2023-01-15 14:18:37');
INSERT INTO `tk_category` VALUES (18, 1, 0, 1, 'Design', 0, '0-1', 'App', '', 0, '', 0, 2, NULL, '2023-01-15 14:18:37');
INSERT INTO `tk_category` VALUES (184, 1, 0, 1, 'Design', 0, '0-1', 'logo', '', 0, '', 0, 5, NULL, '2023-01-15 14:18:37');
INSERT INTO `tk_category` VALUES (185, 4, 0, 4, 'Blog', 0, '0-4', '设计理论', '', 0, '设计理论', 0, 0, NULL, '2023-01-15 14:18:37');
INSERT INTO `tk_category` VALUES (188, 4, 0, 4, 'Blog', NULL, '0-4', '好看风云', NULL, NULL, '好看风云', NULL, 6, NULL, '2023-01-15 14:18:37');

-- ----------------------------
-- Table structure for tk_comment
-- ----------------------------
DROP TABLE IF EXISTS `tk_comment`;
CREATE TABLE `tk_comment`  (
                               `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                               `pid` int(10) NULL DEFAULT NULL,
                               `path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `username` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `nid` mediumint(8) UNSIGNED NULL DEFAULT NULL,
                               `bid` mediumint(8) UNSIGNED NULL DEFAULT NULL,
                               `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `author` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `module` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `headimg` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `ip` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `verify` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
                               `isreply` tinyint(1) NULL DEFAULT 0,
                               `ord` tinyint(5) NULL DEFAULT NULL,
                               `status` tinyint(1) NULL DEFAULT 1,
                               `create_time` datetime(0) NULL DEFAULT NULL,
                               `update_time` datetime(0) NULL DEFAULT NULL,
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 119 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tk_contact
-- ----------------------------
DROP TABLE IF EXISTS `tk_contact`;
CREATE TABLE `tk_contact`  (
                               `id` int(10) NOT NULL,
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Table structure for tk_design
-- ----------------------------
DROP TABLE IF EXISTS `tk_design`;
CREATE TABLE `tk_design`  (
                              `id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
                              `colId` smallint(5) NOT NULL,
                              `title` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
                              `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容',
                              `smallimg` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                              `ctime` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建时间',
                              `commentnum` int(10) NOT NULL DEFAULT 1 COMMENT '评论数量',
                              `status` tinyint(1) NOT NULL DEFAULT 1,
                              `likes` int(10) NOT NULL DEFAULT 1 COMMENT '喜欢数量',
                              `author` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                              `inputtime` int(30) NOT NULL,
                              `posid` tinyint(2) NOT NULL DEFAULT 0,
                              `ord` int(10) UNSIGNED NOT NULL DEFAULT 0,
                              PRIMARY KEY (`id`, `author`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 61 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tk_link
-- ----------------------------
DROP TABLE IF EXISTS `tk_link`;
CREATE TABLE `tk_link`  (
                            `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
                            `linktype` tinyint(1) NULL DEFAULT NULL,
                            `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `status` tinyint(1) NULL DEFAULT 1,
                            `ord` tinyint(5) NULL DEFAULT 0,
                            `introduce` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
                            `contact` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `create_time` datetime(0) NULL DEFAULT NULL,
                            `update_time` datetime(0) NULL DEFAULT NULL,
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tk_message
-- ----------------------------
DROP TABLE IF EXISTS `tk_message`;
CREATE TABLE `tk_message`  (
                               `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                               `pid` int(10) NULL DEFAULT NULL,
                               `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `ip` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `headimg` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                               `verify` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                               `isreply` tinyint(1) NULL DEFAULT NULL,
                               `ord` tinyint(1) NULL DEFAULT NULL,
                               `status` tinyint(1) NULL DEFAULT NULL,
                               `create_time` datetime(0) NULL DEFAULT NULL,
                               `update_time` datetime(0) NULL DEFAULT NULL,
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 54 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tk_node
-- ----------------------------
DROP TABLE IF EXISTS `tk_node`;
CREATE TABLE `tk_node`  (
                            `id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
                            `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `status` tinyint(1) NULL DEFAULT NULL,
                            `remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `sort` smallint(6) UNSIGNED NULL DEFAULT NULL,
                            `pid` smallint(6) UNSIGNED NULL DEFAULT NULL,
                            `level` tinyint(1) UNSIGNED NULL DEFAULT NULL,
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tk_password_resets
-- ----------------------------
DROP TABLE IF EXISTS `tk_password_resets`;
CREATE TABLE `tk_password_resets`  (
                                       `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                       `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                                       `created_at` timestamp(0) NULL DEFAULT NULL,
                                       INDEX `tk_password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tk_role
-- ----------------------------
DROP TABLE IF EXISTS `tk_role`;
CREATE TABLE `tk_role`  (
                            `id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
                            `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `pid` smallint(6) NULL DEFAULT NULL,
                            `status` tinyint(1) UNSIGNED NULL DEFAULT NULL,
                            `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tk_role_user
-- ----------------------------
DROP TABLE IF EXISTS `tk_role_user`;
CREATE TABLE `tk_role_user`  (
                                 `role_id` mediumint(9) UNSIGNED NOT NULL,
                                 `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Table structure for tk_system
-- ----------------------------
DROP TABLE IF EXISTS `tk_system`;
CREATE TABLE `tk_system`  (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `domain` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                              `site_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                              `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                              `keyword` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                              `email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                              `contact` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                              `company` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                              `record` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                              `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                              `icp` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                              `address` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                              `tpl` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
                              `allow_comment` tinyint(2) NULL DEFAULT 0,
                              `show_banner` tinyint(2) NULL DEFAULT 0,
                              `status` tinyint(4) NULL DEFAULT 0,
                              `create_time` datetime(0) NULL DEFAULT NULL,
                              `update_time` datetime(0) NULL DEFAULT NULL,
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 103 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tk_system
-- ----------------------------
INSERT INTO `tk_system` VALUES (1, '127.0.0.1:8080', '橘子洲', '橘子洲', '橘子洲', 'akira@163.com', 'alex', '橘子洲', '', '15911006066', '橘子洲', '橘子洲', 'green', 0, 0, 0, NULL, '2023-01-15 14:18:37');

-- ----------------------------
-- Table structure for tk_user
-- ----------------------------
DROP TABLE IF EXISTS `tk_user`;
CREATE TABLE `tk_user`  (
                            `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
                            `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `sex` tinyint(2) NULL DEFAULT NULL,
                            `avatar` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `nickname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `password` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `loginip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `logintime` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `verify` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `isadmin` tinyint(1) NULL DEFAULT NULL,
                            `status` tinyint(1) NULL DEFAULT NULL,
                            `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                            `create_time` datetime(0) NULL DEFAULT NULL,
                            `update_time` datetime(0) NULL DEFAULT NULL,
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tk_user
-- ----------------------------
INSERT INTO `tk_user` VALUES (1, 'admin', 1, '', '杰克', '大风起兮云飞扬，威加海内兮归故乡\r\n', '$2a$14$W7LKAr1AId3eYmtIiAtFeeLYhPuRldxziwORiuu2roiN2ojwD36xy', NULL, NULL, NULL, 'jikeytang@163.com', NULL, NULL, '中国 • 上海市 • 浦东新区', '2022-07-05 13:42:22', '2022-07-05 13:42:24');

SET FOREIGN_KEY_CHECKS = 1;