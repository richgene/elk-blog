package routers

import (
    "fmt"
    "gitee.com/jikey/elk-blog/app/controller/admin"
    "gitee.com/jikey/elk-blog/app/controller/home"
    "gitee.com/jikey/elk-blog/middleware"
    "gitee.com/jikey/elk-blog/pkg/utils"
    "github.com/flosch/pongo2"
    "github.com/gin-gonic/gin"
    "net/http"
    "strings"
)

func RouterApp() *gin.Engine {
    // gin.SetMode(gin.ReleaseMode)
    gin.SetMode(gin.DebugMode)

    router := gin.Default()
    router.Use(middleware.LoggerToFile())
    // 模板引擎
    router.HTMLRender = utils.NewRender("views/")

    // 静态路由
    router.Static("/public", "./public")
    router.Static("/uploads", "./public/uploads")
    router.StaticFile("/favicon.ico", "./public/common/images/favicon.ico")
    router.StaticFile("/admin/mini", "./public/common/api/init.json")

    // 首页
    indexController := new(admin.Index)
    router.GET("/admin/test", indexController.Test)

    // 不需要经过token验证的路由
    authController := new(admin.Auth)
    router.GET("/admin/login", authController.Login)
    router.POST("/admin/login", authController.SignIn)

    // 管理页面
    back := router.Group("/admin")
    back.Use(middleware.JWTAuth())
    {

        back.GET("/", indexController.Index)
        back.GET("welcome", indexController.Welcome)

        // 上传图片
        back.POST("upload/image", admin.UploadImage)

        // 文章管理
        articleController := new(admin.Article)
        back.GET("article", articleController.Index)
        back.GET("article/category", articleController.Category)
        back.GET("article/category/list", articleController.CategoryList)
        back.GET("article/list", articleController.List)
        back.GET("article/:id", articleController.Detail)
        back.GET("article/edit", articleController.Edit)
        back.GET("article/edit/md", articleController.EditMd)
        back.POST("article/insert", articleController.Insert)
        back.PUT("article/update", articleController.Update)
        back.POST("article/delete", articleController.Destroy)

        // 分类管理
        categoryController := new(admin.Category)
        back.GET("category", categoryController.Index)
        back.GET("category/list", categoryController.List)
        back.POST("category/insert", categoryController.Insert)
        back.PUT("category/update", categoryController.Update)
        back.POST("category/delete", categoryController.Destory)

        // Banner管理
        bannerController := new(admin.Banner)
        back.GET("banner", bannerController.Index)
        back.GET("banner/list", bannerController.List)
        back.POST("banner/insert", bannerController.Insert)
        back.PUT("banner/update", bannerController.Update)
        back.POST("banner/delete", bannerController.Destroy)

        // System管理
        systemController := new(admin.System)
        back.GET("system/setting", systemController.Setting)
        back.GET("system/setting/list", systemController.SettingList)
        back.PUT("system/setting/update", systemController.SettingUpdate)
        back.GET("system/bak", systemController.Bak)
        back.GET("system/restore", systemController.Restore)

        serverController := new(admin.Server)
        back.GET("system/monitor", serverController.List)

        // 留言管理
        messageController := new(admin.Message)
        back.GET("message", messageController.Index)
        back.GET("message/list", messageController.List)
        back.POST("message/delete", messageController.Destory)

        // 评论管理
        commentController := new(admin.Comment)
        back.GET("comment", commentController.Index)
        back.GET("comment/list", commentController.List)
        back.POST("comment/insert", commentController.Insert)
        back.POST("comment/delete", commentController.Destory)

        // 友情链接
        linkController := new(admin.Link)
        back.GET("link", linkController.Index)
        back.GET("link/list", linkController.List)
        back.POST("link/insert", linkController.Insert)
        back.PUT("link/update", linkController.Update)
        back.POST("link/delete", linkController.Destory)

        // 用户管理
        userController := new(admin.User)
        back.GET("user", userController.Index)
        back.GET("users", userController.Users)
        back.GET("users/list", userController.List)
        back.GET("user/password", userController.Password)
        back.GET("user/info", userController.Info)
        back.POST("user/insert", userController.Insert)
        back.PUT("user/update", userController.Update)
        back.POST("user/delete", userController.Destory)

        // 关于我
        aboutController := new(admin.About)
        back.GET("about", aboutController.Index)
        back.PUT("about/update", aboutController.Update)
        back.GET("about/info", aboutController.Detail)
    }

    // 前台页面
    front := router.Group("")
    {
        // 首页
        homeController := new(home.Index)
        front.GET("/", homeController.Index)
        front.GET("/test", homeController.Test)

        // 获取验证码
        front.GET("/verify", homeController.GetVerify)

        // 博客
        blogController := new(home.Article)
        front.GET("/article", blogController.Index)
        front.GET("/article/:id", blogController.Detail)
        front.GET("/article/archive/:id", blogController.Archive)
        front.GET("/article/category/:id", blogController.Category)
        front.GET("/search", blogController.Index)
        front.POST("/article/:id/likes", blogController.Likes)

        // 标签
        tagsController := new(home.Tags)
        front.GET("/tags", tagsController.Index)

        // 评论
        commentController := new(home.Comment)
        front.GET("/comment/list", commentController.List)
        front.POST("/comment/add", commentController.Add)

        // 留言板
        messageController := new(home.Message)
        front.GET("/message", messageController.Index)
        front.POST("/message/add", messageController.Add)

        // 撰稿人
        writerController := new(home.Writer)
        front.GET("/writer", writerController.Index)

        // 关于我
        aboutController := new(home.About)
        front.GET("/about", aboutController.Index)
    }

    // 全局处理404
    router.NoRoute(func(c *gin.Context) {
        path := strings.Split(c.Request.URL.Path, "/")
        fmt.Println("fmt-path: ", path)

        c.HTML(http.StatusOK, "home/errors/404.html", pongo2.Context{
            "active": "index",
            "module": c.Request.URL.Path,
        })
    })

    return router
}
