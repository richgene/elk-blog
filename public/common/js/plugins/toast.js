;(function (win, $) {

    /**
     * 轻提示
     * @param options
     * @constructor
     */
    var Toast = function (options) {
        var _defaults = {
            types: [
                'primary',
                'secondary',
                'info',
                'success',
                'warning',
                'danger'
            ],
            positions: [
                'top-left',
                'top-center',
                'top-right',
                'bottom-left',
                'bottom-center',
                'bottom-right'
            ]
        }
        if (typeof options === 'string') {
            _defaults.message = options
        }

        if (typeof arguments[1] === 'function') {
            _defaults.callback = arguments[1]
        }

        this.settings = $.extend({}, _defaults, options)
        this.body = $(document.body)
        this.position = this.settings.position ? this.settings.position.split('-') : ['top']

        this.init()

        this.settings.callback && this.settings.callback()
    }

    Toast.prototype = {
        init: function () {
            this.render()
        },
        /**
         * 渲染
         */
        render: function () {
            var message = this.settings.message || '',
                tpl = '' +
                    '<div class="app-toast">' +
                    '  <div class="app-toast-text">' + message + '</div>' +
                    '</div>',
                props = {
                    class: this.position.join(' ')
                },
                target = $(this.settings.target)

            if (target.length) {
                this.element = target
                target.fadeIn()
            } else {
                this.element = $(tpl).addClass(props.class).appendTo('body').fadeIn()
            }

            this.hide()
        },
        /**
         * 显示
         */
        show: function () {
            this.element.show()
            this.hide()
        },
        /**
         * 隐藏
         */
        hide: function () {
            var self = this

            setTimeout(function () {
                self.element.fadeOut()
            }, 3000)
        }
    }

    /**
     * 通过jQuery插件方式调用
     * $('#btn').toast(option)
     * @param option
     * @returns {*}
     */
    $.fn.toast = function (option) {
        return this.each(function () {
            var scope = $(this),
                data = scope.data(),
                instance = scope.data('lw.toast'),
                options = $.extend({}, data, option)

            scope.on('click', function () {
                if (!instance) {
                    scope.data('lw.toast', (instance = new Toast(options)))
                } else {
                    instance.show()
                }
            })
        })
    }

    /**
     * 通过命名空
     * app.toast(option)间方式调用
     * @param option
     */
    app.toast = function () {
        var arg = arguments,
            _defaults = {}

        if (typeof arg[0] === 'string') {
            _defaults.message = arg[0]
        } else {
            alert('缺少参数')
        }

        if (typeof arg[1] === 'function') {
            _defaults.callback = arg[1]
        }

        new Toast(_defaults)
    }

    /**
     * 被动的调用
     * <button type="button" data-target="#myToast" data-toggle="toast">提示</button>
     */
    $(document).on('click.lw.toast.data-api', '[data-toggle="toast"]', function () {
        var scope = $(this),
            target = $(scope.data('target')),
            options = scope.data()

        new Toast(options)
    })

}(window, jQuery))
