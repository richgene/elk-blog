/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : elk-blog

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2022-07-30 08:21:33
*/

-- create database elk-blog
create database `elk-blog` default character set utf8mb4 collate utf8mb4_general_ci;

use elk-blog;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tk_about
-- ----------------------------
DROP TABLE IF EXISTS `tk_about`;
CREATE TABLE `tk_about` (
  `id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tk_banner
-- ----------------------------
DROP TABLE IF EXISTS `tk_banner`;
CREATE TABLE `tk_banner` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `modelId` int(10) NOT NULL,
  `smallimg` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `ord` int(8) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for tk_blog
-- ----------------------------
DROP TABLE IF EXISTS `tk_blog`;
CREATE TABLE `tk_blog` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `content` text,
  `md` text,
  `keywords` varchar(100) DEFAULT NULL,
  `smallimg` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `ctime` varchar(30) DEFAULT NULL,
  `username` char(20) DEFAULT NULL,
  `author` varchar(30) DEFAULT NULL,
  `listorder` smallint(5) unsigned DEFAULT '0',
  `posid` tinyint(4) NOT NULL DEFAULT '0',
  `ord` tinyint(3) DEFAULT NULL,
  `views` int(10) DEFAULT NULL COMMENT '浏览量',
  `likes` int(10) DEFAULT NULL COMMENT '点赞',
  `status` tinyint(4) DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tk_category
-- ----------------------------
DROP TABLE IF EXISTS `tk_category`;
CREATE TABLE `tk_category` (
  `colId` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `colPid` smallint(5) NOT NULL,
  `typeid` smallint(3) NOT NULL DEFAULT '0',
  `modelid` smallint(5) NOT NULL,
  `model` varchar(20) NOT NULL,
  `picId` smallint(5) DEFAULT NULL,
  `colPath` varchar(100) NOT NULL,
  `colTitle` varchar(100) NOT NULL,
  `thumb` varchar(150) DEFAULT NULL,
  `asmenu` tinyint(1) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `ord` smallint(3) DEFAULT NULL,
  PRIMARY KEY (`colId`)
) ENGINE=MyISAM AUTO_INCREMENT=204 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tk_category
-- ----------------------------
INSERT INTO `tk_category` VALUES ('1', '0', '0', '1', 'Design', '0', '0', 'Design', '', '1', '设计1', '1', '0');
INSERT INTO `tk_category` VALUES ('2', '0', '0', '2', 'About', '0', '0', 'About', '', '1', '关于我们', '1', '0');
INSERT INTO `tk_category` VALUES ('3', '0', '0', '3', 'Contact', '0', '0', 'Contact', '', '1', '联系我', '1', '0');
INSERT INTO `tk_category` VALUES ('4', '0', '0', '4', 'Blog', '0', '0', 'Blog', '', '1', '博客', '1', '0');
INSERT INTO `tk_category` VALUES ('5', '1', '0', '1', 'Design', '0', '0-1', 'GUI444', '', '0', '<p>ios android</p>', '1', '1');
INSERT INTO `tk_category` VALUES ('6', '1', '0', '1', 'Design', '0', '0-1', 'WEB', '', '0', 'WEB', '1', '4');
INSERT INTO `tk_category` VALUES ('7', '1', '0', '1', 'Design', '0', '0-1', 'PAINT', '', '0', 'PAINT', '1', '3');
INSERT INTO `tk_category` VALUES ('17', '4', '0', '4', 'Blog', '0', '0-4', '轻慢摄影', '', '0', '轻慢摄影', '0', '0');
INSERT INTO `tk_category` VALUES ('14', '4', '0', '4', 'Blog', '0', '0-4', '快乐绘画', '', '0', '快乐绘画', '0', '0');
INSERT INTO `tk_category` VALUES ('15', '4', '0', '4', 'Blog', '0', '0-4', '收藏点滴', '', '0', '收藏点滴', '0', '0');
INSERT INTO `tk_category` VALUES ('18', '1', '0', '1', 'Design', '0', '0-1', 'App', '', '0', '', '0', '2');
INSERT INTO `tk_category` VALUES ('184', '1', '0', '1', 'Design', '0', '0-1', 'logo', '', '0', '', '0', '5');
INSERT INTO `tk_category` VALUES ('185', '4', '0', '4', 'Blog', '0', '0-4', '设计理论', '', '0', '设计理论', '0', '0');
INSERT INTO `tk_category` VALUES ('188', '4', '0', '4', 'Blog', null, '0-4', '好看风云', null, null, '好看风云', null, '6');


-- ----------------------------
-- Table structure for tk_comment
-- ----------------------------
DROP TABLE IF EXISTS `tk_comment`;
CREATE TABLE `tk_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `username` varchar(40) DEFAULT NULL,
  `nid` mediumint(8) unsigned DEFAULT NULL,
  `bid` mediumint(8) unsigned DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `author` varchar(40) DEFAULT NULL,
  `module` varchar(20) DEFAULT NULL,
  `headimg` varchar(250) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `verify` varchar(32) DEFAULT NULL,
  `content` text,
  `isreply` tinyint(1) DEFAULT '0',
  `ord` tinyint(5) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tk_contact
-- ----------------------------
DROP TABLE IF EXISTS `tk_contact`;
CREATE TABLE `tk_contact` (
  `id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tk_design
-- ----------------------------
DROP TABLE IF EXISTS `tk_design`;
CREATE TABLE `tk_design` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `colId` smallint(5) NOT NULL,
  `title` varchar(80) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `smallimg` varchar(100) NOT NULL,
  `ctime` varchar(30) NOT NULL COMMENT '创建时间',
  `commentnum` int(10) NOT NULL DEFAULT '1' COMMENT '评论数量',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `likes` int(10) NOT NULL DEFAULT '1' COMMENT '喜欢数量',
  `author` varchar(30) NOT NULL,
  `inputtime` int(30) NOT NULL,
  `posid` tinyint(2) NOT NULL DEFAULT '0',
  `ord` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`author`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tk_link
-- ----------------------------
DROP TABLE IF EXISTS `tk_link`;
CREATE TABLE `tk_link` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `linktype` tinyint(1) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `ord` tinyint(5) DEFAULT '0',
  `introduce` text,
  `contact` varchar(30) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tk_message
-- ----------------------------
DROP TABLE IF EXISTS `tk_message`;
CREATE TABLE `tk_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `headimg` varchar(250) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `content` text NOT NULL,
  `verify` varchar(32) DEFAULT NULL,
  `isreply` tinyint(1) DEFAULT NULL,
  `ord` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tk_node
-- ----------------------------
DROP TABLE IF EXISTS `tk_node`;
CREATE TABLE `tk_node` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `remark` varchar(50) DEFAULT NULL,
  `sort` smallint(6) unsigned DEFAULT NULL,
  `pid` smallint(6) unsigned DEFAULT NULL,
  `level` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tk_password_resets
-- ----------------------------
DROP TABLE IF EXISTS `tk_password_resets`;
CREATE TABLE `tk_password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `tk_password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tk_role
-- ----------------------------
DROP TABLE IF EXISTS `tk_role`;
CREATE TABLE `tk_role` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `pid` smallint(6) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tk_role_user
-- ----------------------------
DROP TABLE IF EXISTS `tk_role_user`;
CREATE TABLE `tk_role_user` (
  `role_id` mediumint(9) unsigned NOT NULL,
  `user_id` char(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tk_user
-- ----------------------------
DROP TABLE IF EXISTS `tk_user`;
CREATE TABLE `tk_user` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) DEFAULT NULL,
  `sex` tinyint(2) DEFAULT NULL,
  `avatar`  varchar(200)  DEFAULT NULL,
  `nickname` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `password` char(255) DEFAULT NULL,
  `loginip` varchar(20) DEFAULT NULL,
  `logintime` varchar(50) DEFAULT NULL,
  `verify` varchar(32) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `isadmin` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tk_user
-- ----------------------------
INSERT INTO `tk_user` VALUES ('1', 'admin', '1', '杰克', '大风起兮云飞扬，威加海内兮归故乡\r\n', '$2a$14$ePl.tGVMczS/DrvMXNl77u3R4U0SBWXhoOZu8GYe2bqwUckRHRrzC', null, null, null, 'jikeytang@163.com', null, null, '中国 • 上海市 • 浦东新区', '2022-07-05 13:42:22', '2022-07-05 13:42:24');
INSERT INTO `tk_user` VALUES ('2', 'test', null, null, null, '$2a$14$otb9WpTUmSulfMlc526hhe5IeTqFGM5P5hhc1YjT20Pl9j5UGxgvy', null, null, null, null, null, null, null, null, null);
