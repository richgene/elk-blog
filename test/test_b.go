package main

import (
	"fmt"
	"path/filepath"
	"sync"
	"time"
)

// 婷婷的淘宝客户端和web端都会指向婷婷这一个人
type Woman struct {
	name string
}

var (
	once sync.Once
	ting *Woman
	ti1  = int64(0)
	long = 0
)

func getTing() *Woman {
	once.Do(func() {
		ting = new(Woman)
		ting.name = "tingting"
		fmt.Println("newtingting")
	})
	fmt.Println("gettingting")
	return ting
}

func main() {
	for i := 0; i < 3; i++ {
		_ = getTing()
	}
	now := time.Now()
	fileExt := ".jpg"
	//文件存放路径
	fileDir := fmt.Sprintf("articles")

	//文件名称
	timeStamp := now.Unix()
	fileName := fmt.Sprintf("%d%s", timeStamp, fileExt)
	// 文件key
	fileKey := filepath.Join(fileDir, fileName)
	fmt.Println(fileKey)
	t1 := time.Now().Unix()
	if ti1 != t1 { //如果时间戳不一样,那么归零
		long = 0
	}
	ti1 = t1
	long++
	obj := fmt.Sprintf("article/%d%d.png", t1, long)
	fmt.Println(obj)
}
