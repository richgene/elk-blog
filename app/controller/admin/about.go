package admin

import (
	"gitee.com/jikey/elk-blog/app/model"
	"gitee.com/jikey/elk-blog/app/service"
	"gitee.com/jikey/elk-blog/pkg/e"
	"gitee.com/jikey/elk-blog/pkg/response"
	"github.com/flosch/pongo2"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"strconv"
)

type About struct {
	Base
}

// Index 文章新增、编辑
func (b *About) Index(c *gin.Context) {
	c.HTML(200, "admin/about/index.html", pongo2.Context{
		"title": "Welcome11222!",
		"greet": "hello",
		"obj":   "world",
	})
}

// Detail 详情
func (b *About) Detail(c *gin.Context) {
	data := make(map[string]interface{})

	id, _ := strconv.Atoi(c.Query("id"))
	data["rows"], _ = service.About.AboutDetail(id)

	response.Success(c, data)
}

// Update 修改
func (b *About) Update(c *gin.Context) {
	blog := &model.About{}
	id_ := c.DefaultPostForm("id", strconv.Itoa(1))
	if err := c.ShouldBind(&blog); err != nil {
		c.JSON(400, gin.H{
			"err": err.Error(),
		})
		return
	}

	//id := strconv.Itoa(id_)
	blog.BaseModel.Id, _ = strconv.Atoi(id_)
	err := service.About.Update(id_, blog)

	if err != nil {
		logrus.Error("修改失败", err)
		response.Fail(c, e.ErrorUpdate)
		return
	}

	response.Success(c, blog)
}
