package admin

import (
	"gitee.com/jikey/elk-blog/app/model"
	"gitee.com/jikey/elk-blog/app/service"
	"gitee.com/jikey/elk-blog/pkg/e"
	"gitee.com/jikey/elk-blog/pkg/response"
	"gitee.com/jikey/elk-blog/setting"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"strconv"
	"strings"
)

type Message struct {
	Base
}

func (m *Message) Index(c *gin.Context) {
	data := make(map[string]interface{})
	response.HTML(c, "message/index", data)
}

// List 列表
func (m *Message) List(c *gin.Context) {
	data := make(map[string]interface{})

	pageNum, _ := strconv.Atoi(c.Query("page"))

	title := c.Query("content")
	maps := "content like '%" + title + "%'"

	data["rows"] = service.Message.List(pageNum, setting.PageSize, maps)
	data["total"] = service.Message.Total(maps)

	response.Success(c, data)
}

// Insert 新增保存
func (m *Message) Insert(c *gin.Context) {
	banner := &model.Message{}

	if err := c.ShouldBind(&banner); err != nil {
		c.JSON(400, gin.H{
			"err": err.Error(),
		})
		return
	}

	err := service.Message.Create(banner)

	if err != nil {
		logrus.Error("修改失败", err)
		response.Fail(c, e.ErrorUpdate)
		return
	}

	response.Success(c, banner)
}

// Destory 删除
func (m *Message) Destory(c *gin.Context) {
	id := strings.Split(c.PostForm("id"), ",")

	if row := service.Message.Delete(id); row > 0 {
		response.Success(c, id)
		return
	}

	response.Fail(c, e.ErrorDelete)
}
