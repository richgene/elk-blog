package admin

import (
	"gitee.com/jikey/elk-blog/setting"
	"github.com/gin-gonic/gin"
	"reflect"
	"strings"
)

type ControllerInterface interface {
	beforeAction(c *gin.Context) error
	afterAction(c *gin.Context) error
}

type Controller struct {
	actionName string
	viewDir    string              // view 默认文件夹（相对路径）
	this       ControllerInterface // 实例
}

var Site = setting.Config.Site

func (ctrl *Controller) Init(this ControllerInterface) error {
	ctrl.this = this
	className := reflect.TypeOf(ctrl.this).String()
	className = className[strings.LastIndex(className, ".")+1:]
	ctrl.actionName = strings.TrimRight(className, "Controller")
	return nil
}

/*
*
action调用前回调
*/
func (ctrl *Controller) beforeAction(c *gin.Context) error {
	return nil
}

/*
*
action调用后回调
*/
func (ctrl *Controller) afterAction(c *gin.Context) error {
	return nil
}
