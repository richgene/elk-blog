package home

import (
	"gitee.com/jikey/elk-blog/pkg/response"
	"github.com/gin-gonic/gin"
)

type Tags struct {
	Controller
}

func (tags *Tags) Index(c *gin.Context) {
	data := make(map[string]interface{})

	response.Render(c, "tags/index", data)
}
