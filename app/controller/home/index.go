package home

import (
	"gitee.com/jikey/elk-blog/app/service"
	"gitee.com/jikey/elk-blog/pkg/response"
	"gitee.com/jikey/elk-blog/pkg/utils"
	"github.com/flosch/pongo2"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type Index struct {
	Controller
}

var Elk map[interface{}]interface{}

func init() {
	index := &Index{}
	_ = index.Init(index)
	index.Menu()
	Elk = make(map[interface{}]interface{})
	Elk["Menu"] = index.Data["Menu"]
	Elk["ArticleCategory"] = index.Data["ArticleCategory"]
}

func (index *Index) Index(c *gin.Context) {
	data := make(map[string]interface{})

	data["site"] = service.System.GetSystem(1)
	data["active"] = "index"
	data["advs"] = service.Banner.List("")
	data["link"] = service.Link.List(1, 100, "")
	data["archiveList"] = service.Article.ArticleArchiveList()
	data["articleList"] = service.Article.ArticleList(1, 10, "tk_article.status = 1")
	data["menu"] = index.Data["Menu"]
	data["articleCategory"] = service.Category.GetArticleCategoryList("4")
	data["time"] = utils.GetTime()

	response.Render(c, "index/index", data)
}

func (index *Index) Test(c *gin.Context) {
	c.HTML(200, "home/test/test.html", pongo2.Context{
		"title": "topic",
		"greet": "111",
		"obj":   "wo222rld",
	})
}

// GetVerify 获取验证码
func (index *Index) GetVerify(c *gin.Context) {
	id, b64s, err := utils.GenerateCaptcha()
	data := map[string]interface{}{"captchaId": id, "captchaImg": b64s}

	logrus.Error(err)

	response.Success(c, data)
}
