package home

import (
	"gitee.com/jikey/elk-blog/app/service"
	"gitee.com/jikey/elk-blog/pkg/response"
	"github.com/gin-gonic/gin"
)

type Category struct {
	Controller
}

func (category *Category) Index(c *gin.Context) {
	data := make(map[string]interface{})

	data["list"] = service.Article.ArticleList(1, 10, nil)
	data["link"] = service.Link.List(1, 100, "")
	data["site"] = service.System.GetSystem(1)

	response.Render(c, "article/index", data)
}
