package service

import (
	"gitee.com/jikey/elk-blog/app/model"
	db "gitee.com/jikey/elk-blog/app/model"
)

type link struct {
}

var Link *link

func (l *link) List(PageNum int, PageSize int, maps interface{}) (link []*model.Link) {
	db.Mysql.Where(maps).Offset((PageNum - 1) * PageSize).Limit(PageSize).Order("ord asc, id asc").Find(&link)
	return
}

func (l *link) Create(model *model.Link) (err error) {
	err = db.Mysql.Create(model).Error

	return err
}

// Total 获取 blog 总记录数
func (l *link) Total(maps interface{}) (count int) {
	db.Mysql.Model(&link{}).Where(maps).Count(&count)
	return
}

// Update 修改
func (l *link) Update(id string, model *model.Link) (err error) {
	err = db.Mysql.Model(&model).Where("id = ? ", id).Updates(model).Error

	return err
}

// Delete 删除
func (l *link) Delete(id string) bool {
	db.Mysql.Where("id = ?", id).Delete(&link{})

	return true
}
