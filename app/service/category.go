package service

import (
	"gitee.com/jikey/elk-blog/app/model"
	db "gitee.com/jikey/elk-blog/app/model"
)

type category struct {
}

var Category *category

// GetArticleCategoryList 获取 Article 分类列表
func (c *category) GetArticleCategoryList(modelId string) (category []*model.Category) {
	// var Total []int

	db.Mysql.Select("concat(colPath,'-', colId) as bPath, colPid, colId, colTitle, description, ord, model, modelid").Where("modelid = ?", modelId).Order("ord ASC").Find(&category)
	// db.Mysql.Model(&category).Select("count(*) as Total").Pluck("Total", &Total)

	return
}

// Create 新增
func (c *category) Create(model *model.Category) (err error) {
	err = db.Mysql.Create(model).Error

	return err
}

// Update 修改
func (c *category) Update(colId string, model *model.Category) (err error) {
	err = db.Mysql.Model(&model).Where("colId = ? ", colId).Updates(model).Error

	return err
}

// Delete 删除
func (c *category) Delete(id string) (rowsAffected int64) {
	result := db.Mysql.Where("colId = ?", id).Delete(&category{})

	return result.RowsAffected
}
