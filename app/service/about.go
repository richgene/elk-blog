package service

import (
	"gitee.com/jikey/elk-blog/app/model"
	db "gitee.com/jikey/elk-blog/app/model"
)

type about struct {
}

var About *about

// GetBy 查询
func (b *about) GetBy(field, value string) (blog model.About) {
	db.Mysql.Where("? = ?", field, value).First(&blog)
	return
}

// AboutDetail 获取 about 详情
func (b *about) AboutDetail(id int) (mblog model.About, err error) {
	err = db.Mysql.First(&mblog, id).Error
	db.Mysql.Model(&mblog).Where("id = ?", id)
	return
}

// Create 新增
func (b *about) Create(model *model.About) (err error) {
	err = db.Mysql.Omit("ColTitle").Create(model).Error

	return err
}

// Update 修改
func (b *about) Update(id string, model *model.About) (err error) {
	err = db.Mysql.Model(&model).Where("id = ? ", id).Updates(model).Error

	return err
}

// UpdateField 修改某一字段
func (b *about) UpdateField(id string, key string, value interface{}) {
	mBlog := &model.About{}
	db.Mysql.Model(&mBlog).Where("id = ?", id).Update(key, value)
}
