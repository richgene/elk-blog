package service

import (
	"errors"
	"fmt"
	"gitee.com/jikey/elk-blog/app/model"
	db "gitee.com/jikey/elk-blog/app/model"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

type user struct {
}

var User *user

type UserInfoMap map[string]interface {
}

func NewUserInfoMap() UserInfoMap {
	return make(UserInfoMap)
}

func (um UserInfoMap) GetUserName() string {
	if um["username"] == nil {
		return ""
	}

	return um["username"].(string)
}

func (u *user) Login(username, password string) (userModel *model.User, err error) {
	userModel = &model.User{}
	db.Mysql.Where("username=?", username).Find(userModel)

	if userModel.Id > 0 {
		fmt.Println("password = ", password)
		/*
			if userModel.Password != password {
				userModel = nil
				err = errors.New("账号或密码错误")
				return
			}
		*/
	} else {
		userModel = nil
		err = errors.New("账号或密码错误")
	}

	return
}

// SignIn 登录校验
func (u *user) SignIn(username, password string) (*model.User, int) {
	userModel := &model.User{}
	var passwordErr error

	db.Mysql.Where("username = ?", username).First(userModel)
	passwordErr = bcrypt.CompareHashAndPassword([]byte(userModel.Password), []byte(password))

	if userModel.Id == 0 {
		return userModel, 1002
	}

	if passwordErr != nil {
		logrus.Error("passwordErr: ", passwordErr)
		return userModel, 100
	}

	return userModel, 0
}

// Create 新增
func (u *user) Create(model *model.User) (err error) {
	err = db.Mysql.Create(model).Error

	return err
}

// Get 用户信息
func (u *user) Get(id int) (model.ReqUser, error) {
	reqUser := model.ReqUser{}
	err := db.Mysql.Table("tk_user").Find(&reqUser, id).Error
	return reqUser, err
}

// List 获取 user 列表
func (u *user) List(pageNum, pageSize int, filters interface{}) ([]model.ReqUser, int64) {
	var users []model.ReqUser
	var total int64

	db.Mysql.Model(&users).Where(filters).Count(&total)
	db.Mysql.Where(filters).Offset((pageNum - 1) * pageSize).Limit(pageSize).Find(&users)

	return users, total
}

// Update 修改
func (u *user) Update(id string, model *model.User) (err error) {
	err = db.Mysql.Model(&model).Where("id = ? ", id).Updates(model).Error

	return err
}

// Delete 删除
func (u *user) Delete(id string) bool {
	db.Mysql.Where("id = ?", id).Delete(&user{})

	return true
}
