package model

type Category struct {
	BaseModel
	ColId       int    `gorm:"column:colId;primaryKey" json:"colId" form:"colId"`
	ColTitle    string `gorm:"column:colTitle" json:"colTitle" form:"colTitle"`
	ColPid      string `gorm:"column:colPid" json:"colPid" form:"colPid"`
	ColPath     string `gorm:"column:colPath" json:"colPath" form:"colPath"`
	Model       string `gorm:"column:model" json:"model" form:"model"`
	Modelid     string `gorm:"column:modelid" json:"modelid" form:"modelid"`
	Ord         string `gorm:"column:ord" json:"ord" form:"ord"`
	Description string `gorm:"column:description" json:"description" form:"description"`
}
