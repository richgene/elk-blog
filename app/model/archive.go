package model

type Archive struct {
	BaseModel
	Time  string `gorm:"column:Time" json:"Time"`
	Count string `gorm:"column:Count" json:"Count"`
}
