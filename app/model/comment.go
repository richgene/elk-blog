package model

import (
	"gitee.com/jikey/elk-blog/pkg/utils"
	"github.com/jinzhu/gorm"
)

type Comment struct {
	BaseModel
	Pid      int        `gorm:"column:pid;type:int(10)" json:"pid"`
	Path     string     `gorm:"column:path;type:varchar(100)" json:"path"`
	Username string     `gorm:"column:username;type:varchar(40)" json:"username"`
	Nid      uint       `gorm:"column:nid;type:mediumint(8) unsigned" json:"nid"`
	Bid      uint       `gorm:"column:bid;type:mediumint(8) unsigned" json:"bid"`
	Email    string     `gorm:"column:email;type:varchar(50)" json:"email"`
	Author   string     `gorm:"column:author;type:varchar(40)" json:"author"`
	Module   string     `gorm:"column:module;type:varchar(20)" json:"module"`
	Headimg  string     `gorm:"column:headimg;type:varchar(250)" json:"headimg"`
	Url      string     `gorm:"column:url;type:varchar(200)" json:"url"`
	Ip       string     `gorm:"column:ip;type:varchar(15)" json:"ip"`
	Verify   string     `gorm:"column:verify;type:varchar(32)" json:"verify"`
	Content  string     `gorm:"column:content;type:text" json:"content"`
	Isreply  int        `gorm:"column:isreply;type:tinyint(1);default:0" json:"isreply"`
	Ord      int        `gorm:"column:ord;type:tinyint(5)" json:"ord"`
	Status   int        `gorm:"column:status;type:tinyint(1);default:1" json:"status"`
	Children []*Comment `gorm:"-" json:"children"`
}

// AfterFind 返回前加密数据
func (c *Comment) AfterFind(tx *gorm.DB) (err error) {
	if c.Email != "" {
		c.Email = utils.Md5(c.Email)
	}

	return
}
